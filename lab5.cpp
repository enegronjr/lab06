/**************************
*Eddie Negron
*eddien
*Lab 5
*CPSC 1021-001
*Nushrat Humaira
**************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   Card deck[52];
   Card hand[6];


   for(int i = 0; i < 4; i++){
     for(int j = 0; j < 13; j++){
       //depending on i, it will determine the suit of the card.
       switch (i) {
         case 0:
            deck[j+i*13].suit = SPADES;
            break;
         case 1:
            deck[j+i*13].suit = HEARTS;
            break;
         case 2:
            deck[j+i*13].suit = DIAMONDS;
            break;
         case 3:
            deck[j+i*13].suit = CLUBS;
            break;
       }
       //sets the value of the card to current value of j plus 2
       deck[j+i*13].value = j+2;
       //cout << deck[j].suit << "  " << deck[j].value << "\n";
     }
   }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

   //shuffles 'randomly'. Calls to beginning of array, end of array and
   //random function
   random_shuffle(deck,deck+51,myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    for(int i = 0; i < 5; i++){
      hand[i]=deck[i];
    }

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
    //sorts the cards, calls to beginning of array, end of array, and
    //my sort function.
    sort(hand,hand+5,suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
    for(int i = 0; i < 5; i++){
      // sets width to 10, right aligns, and prints name of card
      cout << setw(10) << right << get_card_name(hand[i]) <<
      //prints " of ", the suit of the card, and a new line.
      " of " << get_suit_code(hand[i]) << endl;
    }



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
/*
*PARAMETERS: Card& lhs: left hand side being evaluated
             Card& rhs: right hand side being evaluated
*RETURN:     true or false depending on which card is greater
*This function determines whether lhs or rhs is greater. First it looks
*at the suit. If the same suit it will check the number value of the card.
*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit){
    return true;
  }else if(rhs.suit < lhs.suit){
    return false;
  }
  else if(lhs.value < rhs.value){
    return true;
  }else{
    return false;
  }
}
/*
*PARAMETERS: Card& c: current card
*RETURN: string of the suit of card.
*This function returns the symbol of the suit of the card
*/
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}
/*
*PARAMETERS: Card& c: current card
*RETURN: string of the value of card.
*This function returns the number of the card if c.value is below 11 and
*returns the name of the value if it is 11 or greater and error if it
*is not valid
*/
string get_card_name(Card& c) {
  if(c.value > 10){
    switch (c.value) {
      case 11:
        return "Jack";
      case 12:
        return "Queen";
      case 13:
        return "King";
      case 14:
        return "Ace";
    }
  }else{
    return to_string(c.value);
  }
  return "Error";
}
